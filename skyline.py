def skyline():
    skyline = {
        1: 0,
        2: 0,
        3: 0,
        4: 0,
        5: 0,
        6: 0,
        7: 0,
        8: 0,
        9: 0
    }
    return skyline
def higher_skyline(buildings):
    skyline1 = skyline()
    for i in buildings:
        for x in range(i[0],i[1]+1):
            skyline1[x] = i[-1]
    return skyline1
def generate_skyline(buildings):
    skyline2 = higher_skyline(buildings)
    print(skyline2)
    i = 1
    lst = []
    while i < len(skyline2.items()):
        if skyline2[i] != skyline2[i + 1]:
            lst.append(i+1)
        i += 1
    return lst
if __name__ == "__main__":
    print(generate_skyline([(2, 8, 3), (4, 6, 5)]))